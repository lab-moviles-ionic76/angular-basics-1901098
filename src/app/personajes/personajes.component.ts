import { PersonajeService } from './personajes.service';
import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import { Subscription } from 'rxjs';
@Component({
 selector: 'app-personajes',//aqui designamos el tag de nuestro componente
 templateUrl: './personajes.component.html', //en este archivo ponemos la estructura de nuestro componente
 styleUrls: ['./personajes.component.css'] //aqui cargamos los estilos que utilizara nuestro componente
})
export class PersonajesComponent implements OnInit, OnDestroy{

  private personajesSubs: Subscription = new Subscription;

  //declaramos una variable booleana para control de despliegue
 activo: boolean = true;
 //declaramos un listado de personajes
 personajes: string[] = [];
 //Creamos un metodo que cambie el valor de nuestra bandera
 consultando: boolean = true;
 constructor(private service: PersonajeService){}
   ngOnInit() {
    this.service.fetchPersonajes();
     this.personajesSubs = this.service.personajesChange.subscribe(personajes => {
    this.personajes = personajes;
    this.consultando = false;
    });
   }
   ngOnDestroy() {
     this.personajesSubs.unsubscribe();
   }
  onRemovePersonaje(name: string){
    this.service.removePersonaje(name);
  }
 onClickActivar(){
  this.activo = !this.activo;
 }
}
